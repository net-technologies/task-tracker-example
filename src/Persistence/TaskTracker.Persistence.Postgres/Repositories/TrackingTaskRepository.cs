using TaskTracker.Core.Models;
using TaskTracker.Core.Repositories;
using TaskTracker.Persistence.Postgres.Configurations;
using System.Data;
using Npgsql;
using Dapper;

namespace TaskTracker.Persistence.Postgres.Repositories;

public sealed class TrackingTaskRepository(PostgresConfigurations configurations) : ITrackingTaskRepository
{
    private IDbConnection OpenedConnection
    {
        get
        {
            var connection = new NpgsqlConnection(configurations.ConnectionString);
            connection.Open();
            return connection;
        }
    }

    public int Add(TrackingTask trackingTask)
    {
        const string query =
        """
        INSERT INTO "tasks" ("title", "description", "created_at")
        VALUES (@Title, @Description, @CreatedAt)
        RETURNING "id";
        """;

        using var connection = OpenedConnection;

        var id = connection.QueryFirst<int>(query, trackingTask);

        return id;
    }

    public void Delete(int id)
    {
        const string query =
        """
        DELETE FROM "tasks"
        WHERE "id" = @Id;
        """;

        using var connection = OpenedConnection;
        connection.Execute(query, new { Id = id });
    }

    public void Update(TrackingTask trackingTask)
    {
        const string query =
        """
        UPDATE "tasks"
        SET "title" = @Title, 
            "description" = @Description, 
            "is_completed" = @IsCompleted, 
            "completed_at" = @CompletedAt
        WHERE "id" = @Id;
        """;

        using var connection = OpenedConnection;
        connection.Execute(query, trackingTask);
    }

    public TrackingTask? GetById(int id)
    {
        const string query =
        """
        SELECT "id" as "Id", 
               "title" as "Title", 
               "description" as "Description", 
               "is_completed" as "IsCompleted", 
               "completed_at" as "CompletedAt",
               "created_at" as "CreatedAt"
        FROM "tasks"
        WHERE "id" = @Id
        LIMIT 1;
        """;

        using var connection = OpenedConnection;
        var trackingTask = connection.QueryFirstOrDefault<TrackingTask>(query, new { Id = id });

        return trackingTask;
    }

    public IReadOnlyCollection<TrackingTask> GetAll()
    {
        const string query =
        """
        SELECT "id" as "Id", 
               "title" as "Title", 
               "description" as "Description", 
               "is_completed" as "IsCompleted", 
               "completed_at" as "CompletedAt",
               "created_at" as "CreatedAt"
        FROM "tasks";
        """;

        using var connection = OpenedConnection;
        var trackingTasks = connection.Query<TrackingTask>(query).ToList();

        return trackingTasks;
    }
}