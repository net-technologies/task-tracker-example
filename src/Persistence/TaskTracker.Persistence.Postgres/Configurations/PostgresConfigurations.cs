namespace TaskTracker.Persistence.Postgres.Configurations;

public sealed class PostgresConfigurations(string connectionString)
{
    internal string ConnectionString { get; } = connectionString;
}