using TaskTracker.Core.Models;
using TaskTracker.Core.Repositories;
using TaskTracker.Persistence.SqlServer.Configurations;
using Microsoft.Data.SqlClient;

namespace TaskTracker.Persistence.SqlServer.Repositories;

public class TrackingTaskRepository(SqlServerConfigurations configurations) : ITrackingTaskRepository
{
    private SqlConnection OpenedConnection
    {
        get
        {
            var connection = new SqlConnection(configurations.ConnectionString);
            connection.Open();
            return connection;
        }
    }

    public int Add(TrackingTask trackingTask)
    {
        const string query =
        """
        INSERT INTO [TrackingTasks] ([Title], [Description], [CreatedAt])
        VALUES (@Title, @Description, @CreatedAt);

        SELECT @@IDENTITY;
        """;

        using var connection = OpenedConnection;
        using var command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@Title", trackingTask.Title);
        command.Parameters.AddWithValue("@Description", trackingTask.Description);
        command.Parameters.AddWithValue("@CreatedAt", trackingTask.CreatedAt);
        using var reader = command.ExecuteReader();
        reader.Read();
        
        var id = Convert.ToInt32(reader[0]);

        return id;
    }

    public void Delete(int id)
    {
        const string query =
        """
        DELETE FROM [TrackingTasks]
        WHERE [Id] = @Id;
        """;

        using var connection = OpenedConnection;
        using var command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@Id", id);
        command.ExecuteNonQuery();
    }

    public void Update(TrackingTask trackingTask)
    {
        const string query =
        """
        UPDATE [TrackingTasks]
        SET [Title] = @Title, 
            [Description] = @Description, 
            [IsCompleted] = @IsCompleted, 
            [CompletedAt] = @CompletedAt
        WHERE [Id] = @Id;
        """;

        using var connection = OpenedConnection;
        using var command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@Id", trackingTask.Id);
        command.Parameters.AddWithValue("@Title", trackingTask.Title);
        command.Parameters.AddWithValue("@Description", trackingTask.Description);
        command.Parameters.AddWithValue("@IsCompleted", trackingTask.IsCompleted);
        command.Parameters.AddWithValue("@CompletedAt", trackingTask.CompletedAt);
        command.ExecuteNonQuery();
    }

    public TrackingTask? GetById(int id)
    {
        const string query =
        """
        SELECT TOP 1 [Id], [Title], [Description], [IsCompleted], [CreatedAt], [CompletedAt]
        FROM [TrackingTasks]
        WHERE [Id] = @Id;
        """;

        using var connection = OpenedConnection;
        using var command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@Id", id);
        using var reader = command.ExecuteReader();
        reader.Read();

        if (!reader.HasRows)
            return null;

        return new TrackingTask
        {
            Id = Convert.ToInt32(reader["Id"]),
            Title = reader["Title"].ToString()!,
            Description = reader["Description"].ToString(),
            IsCompleted = Convert.ToBoolean(reader["IsCompleted"]),
            CreatedAt = Convert.ToDateTime(reader["CreatedAt"]),
            CompletedAt = reader["CompletedAt"] is DBNull ? null : Convert.ToDateTime(reader["CompletedAt"])
        };
    }

    public IReadOnlyCollection<TrackingTask> GetAll()
    {
        const string query =
        """
        SELECT [Id], [Title], [Description], [IsCompleted], [CreatedAt], [CompletedAt]
        FROM [TrackingTasks];
        """;

        using var connection = OpenedConnection;
        using var command = new SqlCommand(query, connection);
        using var reader = command.ExecuteReader();

        var trackingTasks = new List<TrackingTask>();
        while (reader.Read())
        {
            trackingTasks.Add(new TrackingTask
            {
                Id = Convert.ToInt32(reader["Id"]),
                Title = reader["Title"].ToString()!,
                Description = reader["Description"].ToString(),
                IsCompleted = Convert.ToBoolean(reader["IsCompleted"]),
                CreatedAt = Convert.ToDateTime(reader["CreatedAt"]),
                CompletedAt = reader["CompletedAt"] is DBNull ? null : Convert.ToDateTime(reader["CompletedAt"])
            });
        }

        return trackingTasks;
    }
}