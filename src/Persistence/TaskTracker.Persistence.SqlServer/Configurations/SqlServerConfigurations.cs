namespace TaskTracker.Persistence.SqlServer.Configurations;

public sealed class SqlServerConfigurations(string connectionString)
{
    internal string ConnectionString { get; } = connectionString;
}
