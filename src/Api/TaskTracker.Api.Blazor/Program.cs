using TaskTracker.Core.Repositories;
using TaskTracker.Core.Services;
using TaskTracker.Persistence.SqlServer.Configurations;
using TaskTracker.Persistence.SqlServer.Repositories;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

builder.Services.AddSingleton(new SqlServerConfigurations(builder.Configuration.GetConnectionString("Default")!));
builder.Services.AddScoped<ITrackingTaskRepository, TrackingTaskRepository>();
builder.Services.AddTransient<ITrackingTaskService, TrackingTaskService>();

var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();

app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");

app.Run();
