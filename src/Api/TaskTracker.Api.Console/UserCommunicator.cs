using TaskTracker.Core.Models;
using TaskTracker.Core.Services;

public sealed class UserCommunicator(ITrackingTaskService taskService)
{
    public void Start()
    {
        int userChoice;
        do
        {
            Console.WriteLine("1. Add task");
            Console.WriteLine("2. Update task");
            Console.WriteLine("3. Delete task");
            Console.WriteLine("4. Get all tasks");
            Console.WriteLine("5. Get task by id");
            Console.WriteLine("0. Exit");

            var userInput = Console.ReadLine();
            if (!int.TryParse(userInput, out userChoice))
            {
                Console.WriteLine("Invalid input. Please try again.");
                continue;
            }

            PerformAction(userChoice);
        } while (userChoice != 0);
    }

    private void PerformAction(int userChoice)
    {
        switch (userChoice)
        {
            case 1:
                AddTask();
                break;
            case 2:
                UpdateTask();
                break;
            case 3:
                DeleteTask();
                break;
            case 4:
                GetAllTasks();
                break;
            case 5:
                GetTaskById();
                break;
        }
    }

    private void AddTask()
    {
        Console.WriteLine("Enter the task title:");
        var taskName = Console.ReadLine();
        if (string.IsNullOrEmpty(taskName))
        {
            Console.WriteLine("The task name is required.");
            return;
        }

        Console.WriteLine("Enter the task description [Optional]:");
        var taskDescription = Console.ReadLine();

        var task = new TrackingTask { Title = taskName, Description = taskDescription };
        var id = taskService.Add(task);
        Console.WriteLine(id == -1 ? "The task is not valid." : $"The task has been added. Id: {id}");
    }

    private void UpdateTask()
    {
        Console.WriteLine("Enter the task id:");

        var inputId = Console.ReadLine();
        if (!int.TryParse(inputId, out var taskId))
        {
            Console.WriteLine("Invalid task id.");
            return;
        }

        var taskToUpdate = taskService.GetById(taskId);
        if (taskToUpdate == null)
        {
            Console.WriteLine("The task is not found.");
            return;
        }

        Console.WriteLine("Enter the new task name [Optional]:");
        var newTaskName = Console.ReadLine();
        if (!string.IsNullOrWhiteSpace(newTaskName))
            taskToUpdate.Title = newTaskName;

        Console.WriteLine("Enter the new task description [Optional]:");
        var newTaskDescription = Console.ReadLine();
        if (!string.IsNullOrWhiteSpace(newTaskDescription))
            taskToUpdate.Description = newTaskDescription;

        Console.WriteLine("Have you completed the task? [Y/N]");
        var isCompleted = Console.ReadLine();
        if (isCompleted?.ToUpper() == "Y")
        {
            taskToUpdate.IsCompleted = true;
            taskToUpdate.CompletedAt = DateTime.Now;
        }

        taskService.Update(taskToUpdate);
        Console.WriteLine("The task has been updated.");
    }

    private void DeleteTask()
    {
        Console.WriteLine("Enter the task id:");
        var inputId = Console.ReadLine();
        if (!int.TryParse(inputId, out var taskIdToDelete))
        {
            Console.WriteLine("Invalid task id.");
            return;
        }

        taskService.Delete(taskIdToDelete);
        Console.WriteLine("The task has been deleted.");
    }

    private void GetAllTasks()
    {
        var tasks = taskService.GetAll();
        foreach (var t in tasks)
            Console.WriteLine($"Id: {t.Id}, Name: {t.Title}, Description: {t.Description}, CompletedAt: {t.CompletedAt}");
    }

    private void GetTaskById()
    {
        Console.WriteLine("Enter the task id:");
        var inputId = Console.ReadLine();
        if (!int.TryParse(inputId, out var taskId))
        {
            Console.WriteLine("Invalid task id.");
            return;
        }

        var task = taskService.GetById(taskId);
        if (task == null)
        {
            Console.WriteLine("The task is not found.");
            return;
        }

        Console.WriteLine($"Id: {task.Id}, Name: {task.Title}, Description: {task.Description}, CompletedAt: {task.CompletedAt}");
    }
}