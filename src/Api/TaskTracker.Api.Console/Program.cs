﻿using Microsoft.Extensions.Configuration;
using TaskTracker.Core.Services;
using TaskTracker.Persistence.Postgres.Configurations;
using TaskTracker.Persistence.Postgres.Repositories;

var configurations = new ConfigurationBuilder()
    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
    .AddJsonFile("appsettings.json")
    .Build();

Console.WriteLine("Task Tracker");
Console.WriteLine("Press any key to start...");
Console.ReadKey();

var postgresConfigurations = new PostgresConfigurations(configurations.GetConnectionString("Default")!);
var taskRepository = new TrackingTaskRepository(postgresConfigurations);
var taskService = new TrackingTaskService(taskRepository);

var userCommunicator = new UserCommunicator(taskService);
userCommunicator.Start();