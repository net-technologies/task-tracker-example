using TaskTracker.Core.Models;
using TaskTracker.Core.Repositories;
using Microsoft.Extensions.Logging;

namespace TaskTracker.Core.Services;

public interface ITrackingTaskService
{
    /// <summary>
    /// Create a new tracking task.
    /// </summary>
    /// <param name="task">The tracking task model</param>
    /// <returns>
    /// <see cref="InvalidId"/> if the validation is failed or something went wrong.
    /// Otherwise the id of the created task
    /// </returns>
    int Add(TrackingTask task);

    /// <summary>
    /// Update the tracking task.
    /// </summary>
    /// <param name="task">The tracking task model</param>
    void Update(TrackingTask task);

    /// <summary>
    /// Delete the tracking task by id.
    /// </summary>
    /// <param name="id">The id of the tracking task that should be deleted</param>
    void Delete(int id);
    
    /// <summary>
    /// Get all tracking tasks.
    /// </summary>
    /// <returns>The collection of found tracking tasks</returns>
    IReadOnlyCollection<TrackingTask> GetAll();

    /// <summary>
    /// Get the tracking task by id.
    /// </summary>
    /// <param name="id">The id of the tracking task that should be found</param>
    /// <returns>Found tracking task or null</returns>
    TrackingTask? GetById(int id);
}

public sealed class TrackingTaskService(
    ITrackingTaskRepository repository,
    ILogger<TrackingTaskService>? logger = null)
    : ITrackingTaskService
{
    private const int InvalidId = -1;

    /// <inheritdoc />
    public int Add(TrackingTask task)
    {
        try
        {
            if (!IsValid(task))
                return InvalidId;

            var id = repository.Add(task);

            return id;
        }
        catch (Exception ex)
        {
            logger?.LogError(ex, "Failed to create a new tracking task");
            return InvalidId;
        }
    }

    /// <inheritdoc />
    public void Update(TrackingTask task)
    {
        try
        {
            if (!IsValid(task))
                return;

            repository.Update(task);
        }
        catch (Exception ex)
        {
            logger?.LogError(ex, "Failed to update the tracking task");
        }
    }

    /// <inheritdoc />
    public void Delete(int id)
    {
        try
        {
            repository.Delete(id);
        }
        catch (Exception ex)
        {
            logger?.LogError(ex, "Failed to delete the tracking task");
        }
    }

    /// <inheritdoc />
    public TrackingTask? GetById(int id)
    {
        try
        {
            return repository.GetById(id);
        }
        catch (Exception ex)
        {
            logger?.LogError(ex, "Failed to get the tracking task by id");
            return null;
        }
    }

    /// <inheritdoc />
    public IReadOnlyCollection<TrackingTask> GetAll()
    {
        try
        {
            return repository.GetAll();
        }
        catch (Exception ex)
        {
            logger?.LogError(ex, "Failed to get all tracking tasks");
            return Array.Empty<TrackingTask>();
        }
    }

    /// <summary>
    /// Check if the tracking task is valid.
    /// Tracking task must:
    /// <list type="bullet">
    /// <item>
    /// <description>Have a title</description>
    /// </item>
    /// <item>
    /// <description>Have a title with length less than 100 characters</description>
    /// </item>
    /// </list>
    /// </summary>
    /// <param name="task">The tracking task model</param>
    /// <returns>true if valid, false otherwise</returns>
    private static bool IsValid(TrackingTask task)
    {
        if (string.IsNullOrWhiteSpace(task.Title))
            return false;
        
        if (task.Title.Length > 100)
            return false;

        if (task.Description?.Length > 500)
            return false;

        if (task.IsCompleted && task.CompletedAt == default)
            return false;

        if (task.IsCompleted && task.CompletedAt > DateTime.Now)
            return false;   

        return true;
    }
}