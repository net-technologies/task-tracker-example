using TaskTracker.Core.Models;

namespace TaskTracker.Core.Repositories;

public interface ITrackingTaskRepository
{
    int Add(TrackingTask task);
    void Delete(int id);
    void Update(TrackingTask task);
    TrackingTask? GetById(int id);
    IReadOnlyCollection<TrackingTask> GetAll();
}