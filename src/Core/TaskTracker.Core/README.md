# Core of the application

Тут можна ознайомитись із основною логікою застосунку і з деякими абстракціями

В даній збірці представлені [основні моделі](./Models), [сервіси](https://gitlab.com/net-technologies/task-tracker-example/-/tree/main/TaskTracker.Core/Services?ref_type=heads) та [абстракції репозиторіїв](https://gitlab.com/net-technologies/task-tracker-example/-/tree/main/TaskTracker.Core/Repositories?ref_type=heads)